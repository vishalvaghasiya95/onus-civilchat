//
//  DBConstant.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation

let DB_ID                              = "id"
let DB_USER_NAME                       = "username"
let DB_SPOUSE_NAME                     = "spouse_name"
let DB_USER_EMAIL                      = "user_email"
let DB_USER_PASSWORD                   = "user_password"
