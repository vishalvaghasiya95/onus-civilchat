//
//  APIType.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
@objc enum APIType: Int {
    case api = 0
    case User_Login
    case User_Registration
}
