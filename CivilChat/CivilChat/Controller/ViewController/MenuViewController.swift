//
//  MenuViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    var window: UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpButtonClick(_ sender: UIButton) {
    }
    
    @IBAction func termOfServiceButtonClick(_ sender: UIButton) {
    }
    
    @IBAction func privacyButtonClick(_ sender: UIButton) {
    }
    
    @IBAction func versionButtonClick(_ sender: UIButton) {
    }
    
    @IBAction func logOutButtonClick(_ sender: UIButton) {
        UserDefaults.standard.set(nil, forKey: "UserID")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
//        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
//        let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
//        navigationController.viewControllers = [rootViewController]
//        self.window?.rootViewController = navigationController
    }
    
}
