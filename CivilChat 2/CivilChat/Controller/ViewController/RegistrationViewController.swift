//
//  RegistrationViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import Alamofire
class RegistrationViewController: UIViewController , UtilityDelegate{

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailAddressTextField: UITextField!
    @IBOutlet weak var devorceNumberTexfield: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet weak var spouseNameTextField: UITextField!
    @IBOutlet var spouseTextField: UITextField!
    
    
    var emailTrim:String = String()
    var spouseEmail_trim:String = String()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registrationButtonClick(_ sender: UIButton) {
        if validation() {
            Utility.showProgress("")
            emailTrim = emailAddressTextField.text!
            spouseEmail_trim = spouseTextField.text!
            
            let dotNot:CharacterSet = CharacterSet(charactersIn: "@._-")
            emailTrim = (emailTrim.components(separatedBy: dotNot) as NSArray).componentsJoined(by: "")
            
            spouseEmail_trim = (spouseEmail_trim.components(separatedBy: dotNot) as NSArray).componentsJoined(by: "")
            
            let params = [DB_USER_NAME : emailTrim,
                         DB_SPOUSE_NAME : spouseEmail_trim,
                         DB_USER_EMAIL : emailAddressTextField.text ?? "",
                         DB_USER_PASSWORD : passwordTextField.text ?? ""
                ] as [String : Any]
            
            let headers = [ "Content-Type": "form/data" ]
            
            utility.UserRegisterDetails(URL: URL_REGISTRATION, parameter: params, headers: headers, apiType: .User_Registration)
//            print(param)
//            print(URL_REGISTRATION)
//            let headers = [ "Content-Type": "application/json" ]
//            var request =  URLRequest(url: URL(string: URL_REGISTRATION)!)
//            request.httpMethod = "POST"
//            request.httpBody = try? JSONSerialization.data(withJSONObject: param)
//            request.allHTTPHeaderFields = headers
//
//            utility.UserRegisterDetails(request: request, apiType: .User_Registration)
        }
    }
    
    //MARK:- Validation Function
    func validation() -> Bool{
        if self.emailAddressTextField.text != "" && self.spouseTextField.text != "" && self.nameTextField.text != "" && self.passwordTextField.text != "" {
            if isValidEmail(testStr: self.emailAddressTextField.text!) && isValidEmail(testStr: self.spouseTextField.text!){
                return true
            }
            else{
                if isValidEmail(testStr: self.emailAddressTextField.text!){
                }
                else{
                    Utility.showAlert("Message", message: "Please enter valid email address.", viewController: self)
                }
                
                if isValidEmail(testStr: self.spouseTextField.text!){
                }
                else{
                    Utility.showAlert("Message", message: "Please enter valid spouse email address.", viewController: self)
                }
                return false
            }
        }
        else if self.emailAddressTextField.text == "" && self.spouseTextField.text == "" && self.nameTextField.text == "" && self.passwordTextField.text == ""{
            Utility.showAlert("Message", message: "all field reuqured.", viewController: self)
            return false
        }
        else if self.emailAddressTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter email address.", viewController: self)
            return false
        }
        else if self.passwordTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter password.", viewController: self)
            return false
        }
        else if self.nameTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter name.", viewController: self)
            return false
        }
        else if self.spouseTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter spouse email address.", viewController: self)
            return false
        }
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_Registration:
                print("Success")
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("Message", message: error, viewController: self)
        }
    }
}
