//
//  MyConverstionViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
//import Alamofire
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
class MyConverstionViewController: UIViewController, UtilityDelegate, UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var popupView: UIView!
    @IBOutlet var messageView: UIView!
    @IBOutlet var conversationTableview: UITableView!
    
    var channelRef: DatabaseReference = Database.database().reference()
    private lazy var messageRef: DatabaseReference = self.channelRef.child("conversation")
    
    private var newMessageRefHandle: DatabaseHandle?
    
    var conversations:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var noConversationFound: UILabel!
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        conversationTableview.delegate = self
        conversationTableview.dataSource = self
        
        self.messageView.layer.cornerRadius = 10.0
        self.messageView.clipsToBounds = true
        self.conversationTableview.isHidden = true
        Utility.showProgress("")
        observeMessages()
        noConversationFound.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
//        if let refHandle = updatedMessageRefHandle {
//            messageRef.removeObserver(withHandle: refHandle)
//        }
    }
    
    private func observeMessages() {
        messageRef = channelRef.child("conversation")
        let messageQuery = messageRef.queryLimited(toLast:10000)
        Utility.dismissProgress()
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            if snapshot.value != nil {
                self.conversationTableview.isHidden = false
                let myConversations:NSDictionary = snapshot.value as! NSDictionary
                if myConversations.count > 0 {
                    Utility.dismissProgress()
                    self.conversations.add(myConversations)
                    self.conversationTableview.reloadData()
                }
                else{
//                    Utility.dismissProgress()
                     self.conversationTableview.isHidden = true
                }
            }
            else{
//                Utility.dismissProgress()
                self.conversationTableview.isHidden = true
            }
        })
//        print("85",conversations.count)
//        if conversations.count == 0 {
//             Utility.dismissProgress()
//            self.conversationTableview.isHidden = true
//        }
    }
    
    //MARK:- Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell") as! ConversationTableViewCell
        let data:NSDictionary = self.conversations[indexPath.row] as! NSDictionary
        cell.nameLabel.text = data.value(forKey: "conversationName") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data:NSDictionary = self.conversations[indexPath.row] as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController1") as! ChatViewController1
        vc.userID = (data.value(forKey: "userId") as? String)!
        vc.channelID = (data.value(forKey: "id") as? String)!
        vc.conversationName = (data.value(forKey: "conversationName") as? String)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
            //do stuff
            completionHandler(true)
            let data:NSDictionary = self.conversations[indexPath.row] as! NSDictionary
            print(data)
            let alert:UIAlertController = UIAlertController(title: "", message: "are you sure want to delete conversation ?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                let id = data.value(forKey: "conversationName")
                if let exerciseName = id {
                    let ref = Database.database().reference().child("conversation")
                    ref.queryOrdered(byChild: "conversationName").queryEqual(toValue: exerciseName).observe(.childAdded, with: { (snapshot) in
                        snapshot.ref.removeValue(completionBlock: { (error, reference) in
                            if error != nil {
                                print("There has been an error:\(error)")
                            }
                            else{
                                print("Delete Success!",indexPath.row)
                                //                            self.conversations.remove(indexPath.row)
                                self.conversations.removeObject(at: indexPath.row)
                                print(self.conversations)
                                self.conversationTableview.reloadData()
                                //                            self.conversationTableview.deleteRows(at: [indexPath], with: .left)
                            }
                        })
                    })
                }
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action) in
            }))
            self.present(alert, animated: true, completion: nil)
        })
        action.image = UIImage(named: "menus.png")
        action.backgroundColor = UIColor(red: 0/255, green: 148/255, blue: 204/255, alpha: 1.0)
        let confrigation = UISwipeActionsConfiguration(actions: [action])
        
        return confrigation
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonClick(_ sender: UIButton) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addConversationButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartConverstionViewController") as! StartConverstionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func okButtonClick(_ sender: UIButton) {
        self.popupView.isHidden = true
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_Registration:
                print("Success")
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("Message", message: error, viewController: self)
        }
    }
    
}
