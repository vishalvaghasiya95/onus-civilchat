//
//  ChatViewController1.swift
//  CivilChat
//
//  Created by admin on 02/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth
import FirebaseCore
class ChatViewController1: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var messageTextView: UIView!
    @IBOutlet var chatTableview: UITableView!
    @IBOutlet var messageTextFields: UITextView!
    
    private let imageURLNotSetKey = "NOTSET"
    private var locationSetKey = "LOCATIONNOTSET"
    
    var userID : String = ""
    var senderId:String = ""
    var conversationName:String = ""
    var channelRef: DatabaseReference = Database.database().reference()
    //    var userObj : User = User()
    
    private lazy var messageRef: DatabaseReference = self.channelRef.child("messages")
    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://civilchat-3ed4c.appspot.com/")
    
    private lazy var userIsTypingRef: DatabaseReference = self.channelRef.child("typingIndicator").child(self.senderId)
    private lazy var usersTypingQuery: DatabaseQuery = self.channelRef.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    
    var curruntUserID:String = String()
    var senderDisplayName:String = String()
    
    var selectedIndex : Int = 0
    let defaults = UserDefaults.standard
    var resolveView = UIView()
    var flag : Bool?
    var channelID:String = String()
    
    var messageDetails:Messages = Messages()
    
    var channel: Channels? {
        didSet {
            title = channel?.name
        }
    }
    
    var addMessage:NSMutableArray = NSMutableArray()
    var addMessageKey:NSMutableArray = NSMutableArray()
    
    var messageIndex:Int = -1
    var bounds = UIScreen.main.bounds
    var screenWidth:CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: Notification.Name("refreshData"), object: nil)
        
        self.messageTextView.layer.cornerRadius = self.messageTextView.frame.height / 2;
        self.messageTextView.clipsToBounds = true
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0).cgColor
        
        screenWidth = bounds.size.width
        
        self.navigationController?.isNavigationBarHidden = true
        self.channelRef = Database.database().reference().child("chaneels/\(channelID)/")
        self.senderId = UserDefaults.standard.value(forKey: "UserID") as! String
        self.senderDisplayName = "test"
        curruntUserID = UserDefaults.standard.value(forKey: "UserID") as! String

        observeMessages()
        self.chatTableview.delegate = self
        self.chatTableview.dataSource = self
        self.chatTableview.reloadData()
        readByMessage()
        
        self.title = conversationName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshData(){
        observeMessages()
        self.chatTableview.delegate = self
        self.chatTableview.dataSource = self
        self.chatTableview.reloadData()
    }
    
    //MARK: Button CLick Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func attachButtonClick(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Capture Image", style: .default, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction(title: "Pick From PhotoGalary", style: .default, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sendMessageButtonClick(_ sender: UIButton) {
        // 1
        let itemRef = messageRef.childByAutoId()
        // 2
        let messageItem = [
            "senderId": senderId,
            "senderName": senderDisplayName,
            "text": self.messageTextFields.text!,
            "read": "0",
            "ismediamessage": "0",
            "receivervisible": "0",
            "sendervisible": "1",
            "timestamp":"\(Date().timeIntervalSince1970 * 1000)"
            ] as [String : Any]
        // 3
        itemRef.setValue(messageItem)
        messageDetails.messageid = itemRef.key
        messageDetails.ismediamessage = "0"
        messageDetails.read = "0"
        messageDetails.senderId = senderId
        messageDetails.senderName = senderDisplayName
        messageDetails.text = self.messageTextFields.text!
        messageDetails.timestamp = "\(Date().timeIntervalSince1970 * 1000)"
        messageDetails.receivervisible = "0"
        messageDetails.sendervisible = "1"
        messageDetails.channelid = channelID
        
        DBManager.shared.insertMessageData(messageData: messageDetails)
        
        self.messageTextFields.text = ""
        self.messageTextFields.resignFirstResponder()
        
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryLimited(toLast:10)
        // messages being written to the Firebase DB
        DispatchQueue.main.async {
            self.newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
                let messageData:NSDictionary = snapshot.value as! NSDictionary
                if messageData.count > 0 {
                    self.chatTableview.delegate = self
                    self.chatTableview.dataSource = self
                }
                self.chatTableview.reloadData()
                self.chatTableview.setNeedsLayout()
                self.listing_scrollToBottom()
                self.chatTableview.setNeedsLayout()
                self.chatTableview.reloadData()
            })
        }
    }
    
    private func observeMessages() {
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryLimited(toLast:10)
        self.addMessage.removeAllObjects()
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            if messageData.count > 0 {
                if  messageData.value(forKey: "senderId") as! String != self.curruntUserID {
                    if messageData.value(forKey: "receivervisible") as! String == "1" {
                        self.addMessage.add(messageData)
                        self.addMessageKey.add(snapshot.key)
                    }
                }
                else{
                    if messageData.value(forKey: "sendervisible") as! String == "1" {
                        self.addMessage.add(messageData)
                        self.addMessageKey.add(snapshot.key)
                    }
                }
                self.chatTableview.delegate = self
                self.chatTableview.dataSource = self
                self.chatTableview.reloadData()
                self.chatTableview.setNeedsLayout()
                self.listing_scrollToBottom()
                self.chatTableview.setNeedsLayout()
            }
            
            //            if let id = messageData["senderId"] as String!, let name = messageData["senderName"] as String!, let text = messageData["text"] as String!, text.count > 0 {
            //                self.addMessage(withId: id, name: name, text: text)
            //            }
            //            else if let id = messageData["senderId"] as String!, let photoURL = messageData["photoURL"] as String! {
            //                if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
            //                    self.addPhotoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
            //
            //                    if photoURL.hasPrefix("gs://") {
            //                        self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
            //                    }
            //                }
            //            }
            //            else if let id = messageData["senderId"], let latitude = messageData["latitude"], let longitude = messageData["longitude"]{
            //
            //                if let mediaItem = JSQLocationMediaItem(maskAsOutgoing: id == self.senderId){
            //                    let lat: Double = Double(latitude)!
            //                    let lon: Double = Double(longitude)!
            //                    let location: CLLocation = CLLocation(latitude: lat, longitude: lon)
            //                    self.addLocationMediaMessage(withId: id, key: snapshot.key, mediaItem: mediaItem, location: location)
            //                    self.fetchLocationDataAtURL(latitude: lat, longitude: lon, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
            //                }
            //            }
            //            else {
            //
            //            }
        })
        
        //        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
        //            let key = snapshot.key
        //            let messageData = snapshot.value as! Dictionary<String, String>
        //
        //            if let photoURL = messageData["photoURL"] as String! {
        //                // The photo has been updated.
        //                if let mediaItem = self.photoMessageMap[key] {
        //                    self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: key)
        //                }
        //            }
        //        })
    }
    
    func listing_scrollToBottom() {
        let delay = 0.1 * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if self.addMessage.count > 0 {
                let lastRowIndexPath = NSIndexPath(row: self.addMessage.count-1, section: 0)
                self.chatTableview.scrollToRow(at: lastRowIndexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: false)
            }
        }
    }
    
    //MARK: message Read Function
    func readByMessage(){
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryOrderedByValue()
        // messages being written to the Firebase DB
        messageQuery.observe(.childAdded, with: { (snapshot) in
            let messageData = snapshot.value as! Dictionary<String, String>
            if((snapshot.value as AnyObject).count != nil)
            {
                if messageData["senderId"] != self.curruntUserID {
                    if messageData["receivervisible"] == "1"{
                        self.messageRef.child(snapshot.key).updateChildValues(["read" : "1"])
                    }
                }
            }
        })
    }
    
    // MARK: Tableview Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MesageTableViewCell") as! MesageTableViewCell
        let leftCell = ((screenWidth - 30) / 2 );
        let rightCell = ((screenWidth - 30 ) / 2);
        let maxWidth = leftCell
        let data:NSDictionary = self.addMessage[indexPath.row] as! NSDictionary
        
        if data.value(forKey: "senderId") as! String != curruntUserID {
            if data.value(forKey: "ismediamessage") as! String == "0" {
                
                cell.leftMessageText.text = data.value(forKey: "text") as! String
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.leftTimeLabel.text = strtime
                
                cell.leftmessageViewBubble.isHidden = false
                cell.leftView.isHidden = false
                cell.rightMessageBubble.isHidden = true
                cell.rightView.isHidden = true
                
//                if cell.leftMessageText.contentSize.width > maxWidth {
//
                    cell.leftView.frame = CGRect(x: 15, y: 5, width: maxWidth, height: cell.leftView.frame.height)

                    cell.leftmessageViewBubble.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.leftMessageText.contentSize.height + 20)
                    cell.leftmessageViewBubble.backgroundColor = DARKBLUE

                    cell.leftMessageText.frame = CGRect(x: 15, y: 5, width: maxWidth - 5 , height: cell.leftMessageText.contentSize.height)

                    cell.leftTimeLabel.frame = CGRect(x: 0, y: cell.leftmessageViewBubble.frame.height, width: maxWidth, height: cell.leftTimeLabel.frame.height)

                    cell.leftmessageViewBubble.layer.cornerRadius = 10.0//(cell.leftmessageViewBubble.frame.height / 2);
                    cell.leftmessageViewBubble.clipsToBounds = true

                    self.chatTableview.rowHeight = cell.leftMessageText.contentSize.height + 45
//                }
//                else{
//                    if cell.leftMessageText.contentSize.height > 50 || cell.leftMessageText.contentSize.width > (leftCell / 2 - 15) {
//                        cell.leftView.frame = CGRect(x: 15, y: 5, width: maxWidth, height: cell.leftView.frame.height)
//
//                        cell.leftmessageViewBubble.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.leftMessageText.contentSize.height + 10)
//                        cell.leftmessageViewBubble.backgroundColor = DARKBLUE
//
//                        cell.leftMessageText.frame = CGRect(x: 15, y: 5, width: maxWidth + 5 , height: cell.leftMessageText.contentSize.height)
//
//                        cell.leftTimeLabel.frame = CGRect(x: 0, y: cell.leftmessageViewBubble.frame.height, width: maxWidth , height: cell.leftTimeLabel.frame.height)
//
//                        cell.leftmessageViewBubble.layer.cornerRadius = 10.0//(cell.leftmessageViewBubble.frame.height / 2);
//                        cell.leftmessageViewBubble.clipsToBounds = true
//
//                        self.chatTableview.rowHeight = cell.leftMessageText.contentSize.height + 45
//                    }
//                    else{
//                        cell.leftView.frame = CGRect(x: 15, y: 5, width: cell.leftMessageText.contentSize.width, height: cell.leftView.frame.height)
//
//                        cell.leftmessageViewBubble.frame = CGRect(x: 0, y: 0, width: cell.leftMessageText.contentSize.width + 20 , height: cell.leftMessageText.contentSize.height + 10)
//                        cell.leftmessageViewBubble.backgroundColor = DARKBLUE
//
//                        cell.leftMessageText.frame = CGRect(x: 15, y: 5, width: cell.leftMessageText.contentSize.width + 5 , height: cell.leftMessageText.contentSize.height)
//
//                        cell.leftTimeLabel.frame = CGRect(x: 0, y: cell.leftmessageViewBubble.frame.height, width: cell.leftMessageText.contentSize.width , height: cell.leftTimeLabel.frame.height)
//
//                        cell.leftmessageViewBubble.layer.cornerRadius = 10.0//(cell.leftmessageViewBubble.frame.height / 2);
//                        cell.leftmessageViewBubble.clipsToBounds = true
//
//                        self.chatTableview.rowHeight = cell.leftMessageText.contentSize.height + 45
//                    }
//                }
            }
        }
        else {
            if data.value(forKey: "ismediamessage") as! String == "0" {
                cell.rightMessageText.text = data.value(forKey: "text") as! String
                
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.righttimeLabel.text = strtime
                
                cell.leftmessageViewBubble.isHidden = true
                cell.leftView.isHidden = true
                cell.rightMessageBubble.isHidden = false
                cell.rightView.isHidden = false
                cell.readImage.isHidden = false
                
                cell.rightView.frame = CGRect(x: rightCell - 20, y: 5, width: maxWidth + 20 , height: cell.rightView.frame.height)
                
                cell.rightMessageBubble.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.rightMessageText.contentSize.height + 20)
                cell.rightMessageBubble.backgroundColor = GREYCOLOR
                
                cell.rightMessageText.frame = CGRect(x: 15, y: 5, width: maxWidth - 5 , height: cell.rightMessageText.contentSize.height + 5)
                
                cell.readImage.frame = CGRect(x: cell.rightMessageBubble.frame.width - cell.readImage.frame.width , y: cell.rightMessageBubble.frame.height , width: cell.readImage.frame.width , height: cell.readImage.frame.height)
                
                cell.righttimeLabel.frame = CGRect(x: 0 , y: cell.rightMessageBubble.frame.height , width: cell.rightMessageBubble.frame.width - cell.readImage.frame.width - 15 , height: cell.righttimeLabel.frame.height)
                
                cell.rightMessageBubble.layer.cornerRadius = 10.0//cell.rightMessageBubble.frame.height / 2;
                cell.rightMessageBubble.clipsToBounds = true
                
                cell.rightDeleteButtonClick.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.rightMessageText.contentSize.height + 20)
                cell.rightDeleteButtonClick.tag = indexPath.row
                cell.rightDeleteButtonClick.addTarget(self, action: #selector(getTag(sender:)), for: .touchDown)
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteMessage(sender:)))
                cell.rightDeleteButtonClick.addGestureRecognizer(longGesture)
                
                self.chatTableview.rowHeight = cell.rightMessageText.contentSize.height + 45
                
//                if cell.rightMessageText.contentSize.width > maxWidth {
//                    cell.rightView.frame = CGRect(x: rightCell - 20, y: 5, width: maxWidth + 20 , height: cell.rightView.frame.height)
//
//                    cell.rightMessageBubble.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.rightMessageText.contentSize.height + 10)
//                    cell.rightMessageBubble.backgroundColor = GREYCOLOR
//
//                    cell.rightMessageText.frame = CGRect(x: 15, y: 5, width: maxWidth - 5 , height: cell.rightMessageText.contentSize.height)
//
//                    cell.readImage.frame = CGRect(x: cell.rightMessageBubble.frame.width - cell.readImage.frame.width , y: cell.rightMessageBubble.frame.height , width: cell.readImage.frame.width , height: cell.readImage.frame.height)
//
//                    cell.righttimeLabel.frame = CGRect(x: 0 , y: cell.rightMessageBubble.frame.height , width: cell.rightMessageBubble.frame.width - cell.readImage.frame.width - 15 , height: cell.righttimeLabel.frame.height)
//
//                    cell.rightMessageBubble.layer.cornerRadius = 10.0//cell.rightMessageBubble.frame.height / 2;
//                    cell.rightMessageBubble.clipsToBounds = true
//
//                    self.chatTableview.rowHeight = cell.rightMessageText.contentSize.height + 45
//                }
//                else{
//
//                    if cell.rightMessageText.contentSize.height > 50 || cell.rightMessageText.contentSize.width > (leftCell / 2 - 15) {
//                        cell.rightView.frame = CGRect(x: rightCell - 20, y: 5, width: maxWidth + 20 , height: cell.rightView.frame.height)
//
//                        cell.rightMessageBubble.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.rightMessageText.contentSize.height + 10)
//                        cell.rightMessageBubble.backgroundColor = GREYCOLOR
//
//                        cell.rightMessageText.frame = CGRect(x: 15, y: 5, width: maxWidth - 5 , height: cell.rightMessageText.contentSize.height)
//
//                        cell.readImage.frame = CGRect(x: cell.rightMessageBubble.frame.width - cell.readImage.frame.width , y: cell.rightMessageBubble.frame.height , width: cell.readImage.frame.width , height: cell.readImage.frame.height)
//
//                        cell.righttimeLabel.frame = CGRect(x: 0 , y: cell.rightMessageBubble.frame.height , width: cell.rightMessageBubble.frame.width - cell.readImage.frame.width - 15 , height: cell.righttimeLabel.frame.height)
//
//                        cell.rightMessageBubble.layer.cornerRadius = 10.0//cell.rightMessageBubble.frame.height / 2;
//                        cell.rightMessageBubble.clipsToBounds = true
//                    }
//                    else{
//                        cell.rightView.frame = CGRect(x: rightCell - 20, y: 5, width: cell.rightMessageText.contentSize.width + 20 , height: cell.rightView.frame.height)
//
//                        cell.rightMessageBubble.frame = CGRect(x: 0, y: 0, width: leftCell + 20 , height: cell.rightMessageText.contentSize.height + 10)
//                        cell.rightMessageBubble.backgroundColor = GREYCOLOR
//
//                        cell.rightMessageText.frame = CGRect(x: 15, y: 5, width: leftCell - 5 , height: cell.rightMessageText.contentSize.height)
//
//                        cell.readImage.frame = CGRect(x: cell.rightMessageBubble.frame.width - cell.readImage.frame.width , y: cell.rightMessageBubble.frame.height , width: cell.readImage.frame.width , height: cell.readImage.frame.height)
//
//                        cell.righttimeLabel.frame = CGRect(x: 0 , y: cell.rightMessageBubble.frame.height , width: cell.rightMessageBubble.frame.width - cell.readImage.frame.width - 15 , height: cell.righttimeLabel.frame.height)
//
//                        cell.rightMessageBubble.layer.cornerRadius = 10.0//cell.rightMessageBubble.frame.height / 2;
//                        cell.rightMessageBubble.clipsToBounds = true
//
//                        self.chatTableview.rowHeight = cell.rightMessageText.contentSize.height + 45
//                    }
//                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func getTag(sender:UIButton){
        self.messageIndex = sender.tag
    }
    
    @objc func deleteMessage(sender:UIGestureRecognizer){
        let data:NSDictionary = self.addMessage[self.messageIndex] as! NSDictionary
        let key = addMessageKey[self.messageIndex] as! String
        
        if data.value(forKey: "receivervisible") as! String == "0"{
            let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete for me", style: .default, handler: { (action) in
                self.deleteForMe(messageKey: key)
            }))
            alert.addAction(UIAlertAction(title: "Delete for everyone", style: .default, handler: { (action) in
                self.deleteEveryone(messageKey: key)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func deleteForMe(messageKey:String){
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryOrderedByValue()
        // messages being written to the Firebase DB
        messageQuery.observe(.childAdded, with: { (snapshot) in
            let messageData = snapshot.value as! Dictionary<String, String>
            if((snapshot.value as AnyObject).count != nil)
            {
                if snapshot.key == messageKey {
                    if messageData["sendervisible"] == "1"{
                        self.messageRef.child(snapshot.key).updateChildValues(["sendervisible" : "0"])
                        self.observeMessages()
                        self.chatTableview.reloadData()
                    }
                }
            }
        })
    }
    
    func deleteEveryone(messageKey:String){
        let ref = Database.database().reference().child("chaneels").child("\(channelID)").child("messages")
        ref.queryOrderedByKey().observe(.childAdded) { (snapshot) in
            if snapshot.key == messageKey {
                snapshot.ref.removeValue(completionBlock: { (error, reference) in
                    if error == nil {
                        DBManager.shared.deleteRecoding(messageID: messageKey)
                        self.observeMessages()
                        self.chatTableview.reloadData()
                    }
                })
            }
        }
    }
}
