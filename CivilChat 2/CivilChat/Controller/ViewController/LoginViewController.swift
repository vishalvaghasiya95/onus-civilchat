//
//  LoginViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewController: UIViewController , UtilityDelegate{

    @IBOutlet var emailTexField: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    
    var usernameTrim:String = String()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button Click Events
    @IBAction func loginButtonClick(_ sender: UIButton) {
        if self.validation() {
            Utility.showProgress("")
            usernameTrim = emailTexField.text!
            let dotNot:CharacterSet = CharacterSet(charactersIn: "@._-")
            usernameTrim = (usernameTrim.components(separatedBy: dotNot) as NSArray).componentsJoined(by: "")
            
            let params = ["username" : usernameTrim,
                         "password" : passwordTextfield.text!] as [String : Any]
            let headers = [ "Content-Type": "form/data" ]
            utility.UserLoginDetails(URL: URL_LOGIN, parameter: params, headers: headers, apiType: .User_Login)
        }
    }
    
    @IBAction func signUpButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotPasswordButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validation() -> Bool{
        if self.emailTexField.text != "" && self.passwordTextfield.text != ""{
            return true
        }
        else if self.emailTexField.text == "" && self.passwordTextfield.text == "" {
             Utility.showAlert("Message", message: "all field reuqured.", viewController: self)
            return false
        }
        else if self.emailTexField.text == "" {
             Utility.showAlert("Message", message: "Please enter email.", viewController: self)
             return false
        }
        else if self.passwordTextfield.text == "" {
             Utility.showAlert("Message", message: "Please enter password.", viewController: self)
             return false
        }
        return false
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_Login:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
                self.navigationController?.pushViewController(vc, animated: true)
                break
            default:
                break
            }
        }
        else{
                Utility.showAlert("Message", message: error, viewController: self)
        }
    }
}

