//
//  DBManager.swift
//  CivilChat
//
//  Created by admin on 04/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import Foundation
import UIKit
import FMDB
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth
import FirebaseCore
class DBManager: NSObject {
    static let shared: DBManager = DBManager()
    
    let databaseFileName = "CivilChat.db"
    var pathToDatabase: String!
    var database: FMDatabase!
    
    let field_messageid = "messageid"
    let field_ismediamessage = "ismediamessage"
    let field_read = "read"
    let field_senderId = "senderId"
    let field_senderName = "senderName"
    let field_text = "text"
    let field_timestamp = "timestamp"
    let field_sendervisible = "sendervisible"
    let field_receivervisible = "receivervisible"
    let field_channelid = "channelid"
    
    var channelRef: DatabaseReference = Database.database().reference()
    private lazy var messageRef: DatabaseReference = self.channelRef.child("messages")
    var curruntUserID:String = String()
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
        
    }
    
    func createDatabase() -> Bool {
        var created = false
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            if database != nil {
                // Open the database.
                if database.open() {
                    let userTableQuery = " CREATE TABLE message_Master (\(field_messageid) text not null ,\(field_ismediamessage) text not null ,\(field_read) text not null , \(field_senderId) text not null , \(field_senderName) text not null , \(field_text) text not null , \(field_timestamp) text not null, \(field_sendervisible) text not null , \(field_receivervisible) text not null , \(field_channelid) text not null)";
                    do {
                        try database.executeUpdate(userTableQuery, values: nil)
                        created = true
                    }
                    catch {
                        print("Could not create table.")
                        print(error.localizedDescription)
                    }
                    database.close()
                }
                else {
                }
            }
        }
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
    func insertMessageData(messageData:Messages){
        if openDatabase() {
            do {
                try database.executeUpdate("INSERT INTO message_Master (\(field_messageid), \(field_ismediamessage), \(field_read), \(field_senderId), \(field_senderName), \(field_text), \(field_timestamp), \(field_sendervisible), \(field_receivervisible), \(field_channelid)) VALUES (?, ?, ? , ? , ? , ? , ? , ? , ? , ?)", values: [messageData.messageid, messageData.ismediamessage, messageData.read, messageData.senderId,messageData.senderName, messageData.text, messageData.timestamp, messageData.sendervisible, messageData.receivervisible, messageData.channelid])
                print("Success")
            }
            catch{
                print("Insert Error:",error.localizedDescription)
            }
            database.close()
        }
        else{
        }
    }
    
    func getMessageList(){
        if openDatabase(){
            let query = "SELECT * FROM message_Master"
            do {
                let results = try database.executeQuery(query, values: nil)
                while results.next() {
                    let recordData:Messages = Messages()
                    recordData.ismediamessage = results.string(forColumn: field_ismediamessage)
                    recordData.messageid = results.string(forColumn: field_messageid)
                    recordData.read = results.string(forColumn: field_read)
                    recordData.receivervisible = results.string(forColumn: field_receivervisible)
                    recordData.senderId = results.string(forColumn: field_senderId)
                    recordData.senderName = results.string(forColumn: field_senderName)
                    recordData.sendervisible = results.string(forColumn: field_sendervisible)
                    recordData.text = results.string(forColumn: field_text)
                    recordData.timestamp = results.string(forColumn: field_timestamp)
                    recordData.channelid = results.string(forColumn: field_channelid)
                    let epocTime = TimeInterval(recordData.timestamp)! / 1000
                    //                    let formatter = DateFormatter()
                    let unixTimestamp = Date(timeIntervalSince1970: epocTime)//NSDate(timeIntervalSince1970: epocTime)
                    
                    let secondDate = unixTimestamp
                    let response = Date().offset(from: secondDate)//NSDate.friendlyIntervalBetweenDates(firstDate, secondDate: secondDate)
                    let distance:Int = Int(response)!
                    if distance > 2 {
                        print("Call Seconds")
                        // Firebase Data Update
                        let id:String = recordData.channelid
                        curruntUserID = UserDefaults.standard.value(forKey: "UserID") as! String
                        self.channelRef = Database.database().reference().child("chaneels/\(id)/")
                        messageRef = channelRef.child("messages")
                        let messageQuery = messageRef.queryOrderedByValue()
                        // messages being written to the Firebase DB
                        messageQuery.observe(.childAdded, with: { (snapshot) in
                            if((snapshot.value as AnyObject).count != nil)
                            {
                                if snapshot.key == recordData.messageid{
                                    // Update firebase Field
                                    self.messageRef.child(snapshot.key).updateChildValues(["receivervisible" : "1"])
                                    DBManager.shared.deleteRecoding(messageID: snapshot.key) // Delete Local Database Record
                                    NotificationCenter.default.post(name: Notification.Name("refreshData"), object: nil)
                                }
                            }
                        })
                        // End Firebase
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
            database.close()
        }
        else{
        }
    }
    
    func deleteRecoding(messageID:String){
        if openDatabase(){
            do {
                try database.executeUpdate("DELETE from message_Master WHERE \(field_messageid)=?", values: [messageID])
                print("Success")
            }
            catch{
            }
        }
        else{
        }
    }
}
extension Date {
    
    func add(minutes: Int) -> Date {
        return Calendar(identifier: .gregorian).date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func offset(from date: Date) -> String {
//        if years(from: date)   > 0 { return "\(years(from: date))y"   }
//        if months(from: date)  > 0 { return "\(months(from: date))M"  }
//        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
//        if days(from: date)    > 0 { return "\(days(from: date))d"    }
//        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))" }
//        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return "0"
    }
}

