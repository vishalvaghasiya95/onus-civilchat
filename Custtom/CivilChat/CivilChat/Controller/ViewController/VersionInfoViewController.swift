//
//  VersionInfoViewController.swift
//  CivilChat
//
//  Created by admin on 03/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import UIKit

class VersionInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
