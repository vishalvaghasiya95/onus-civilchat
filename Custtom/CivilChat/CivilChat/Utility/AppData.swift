//
//  AppData.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
import UIKit

class AppData {
    
    var storyboard : UIStoryboard!;
    var appDelegate: AppDelegate!;
    

    
    static let sharedInstance: AppData = {
        let instance = AppData()
        // some additional setup
        return instance
    }()

}
