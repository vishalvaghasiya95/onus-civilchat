//
//  Utility.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
import UIKit
import Reachability
import SVProgressHUD
import Alamofire
@objc protocol  UtilityDelegate {
    @objc optional func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType)
}

class Utility {
    var window: UIWindow?
    var delegate: UtilityDelegate?
    
    class func showAlert(_ title: String?, message: String?, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Add shadow to View..
    class func addShadow(view: UIView) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 8.0
        //        caDemoButtonLayer?.shadowOpacity = 0.3
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func buttonAddShadow(view: UIButton) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 8.0
        //        caDemoButtonLayer?.shadowOpacity = 0.3
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func showProgress(_ message: String) {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        if(message == "") {
            SVProgressHUD.show()
        }
        else {
            SVProgressHUD.show(withStatus: message)
        }
    }

    class func dismissProgress() {
        SVProgressHUD.dismiss()
    }

    class func checkInternet() -> Bool {
        let networkReachability: Reachability = Reachability.forInternetConnection();
        let networkStatus : NetworkStatus = networkReachability.currentReachabilityStatus();
        if (networkStatus.rawValue == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    //MARK:- API Calling Function
    func UserLoginDetails(URL:String , parameter:[String:Any],headers:[String:String], apiType:APIType){
        Alamofire.request(URL, parameters: parameter, headers: headers).responseJSON { response in
            if Utility.checkInternet(){
                if response.error == nil {
                    let data:NSDictionary = response.result.value as! NSDictionary
                    let message:String = data.value(forKey: "errorMsg") as! String
                    if data.value(forKey: "status")as! Int == 1{
                        let temp:NSDictionary = data.value(forKey: "data") as! NSDictionary
                        print("UserID",temp.value(forKey: "id"))
                        UserDefaults.standard.set(temp.value(forKey: "id"), forKey: "UserID")
                        UserDefaults.standard.set(temp.value(forKey: "username"), forKey: "UserName")
                        self.delegate?.apiCallCompleted!(true, data: nil, error: nil, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message , apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: response.result.error?.localizedDescription, apiType: apiType)
                }
            }
            else{
                self.delegate?.apiCallCompleted!(false, data: nil, error: "Please check internet connection and retry.", apiType: apiType)
            }
        }
    }
    
    func UserRegisterDetails(URL:String , parameter:[String:Any],headers:[String:String], apiType:APIType){
        Alamofire.request(URL, parameters: parameter, headers: headers).responseJSON { response in
            if Utility.checkInternet(){
                if response.error == nil{
                    let data:NSDictionary = response.result.value as! NSDictionary
                    let message:String = data.value(forKey: "errorMsg") as! String
                    if data.value(forKey: "status")as! Int == 1 {
                        let temp:NSDictionary = data.value(forKey: "data") as! NSDictionary
                        self.delegate?.apiCallCompleted!(true, data: nil, error: nil, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message , apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: response.result.error?.localizedDescription, apiType: apiType)
                }
            }
            else{
                self.delegate?.apiCallCompleted!(false, data: nil, error: "Please check internet connection and retry.", apiType: apiType)
            }
        }
    }
}
